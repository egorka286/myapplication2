package com.example.myapplication2;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.UUID;

@Entity
public class Contact {
    @NonNull
    @PrimaryKey
    public String id;
    public String name;
    public String surname;
    public String phone;
    public String email;

    public Contact(@NonNull String name, String surname, String phone, String email) {
        id = UUID.randomUUID().toString();
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.email = email;
    }
}
