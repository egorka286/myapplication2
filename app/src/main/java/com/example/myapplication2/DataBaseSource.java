package com.example.myapplication2;

import android.content.Context;

import androidx.room.Room;

public class DataBaseSource {
    private static final String DATABASE_NAME = "database";
    private static DataBaseSource sInstance = null;
    private final AppDatabase mDatabase;

    private DataBaseSource(Context context) {
        mDatabase = Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();
    }

    public static DataBaseSource getInstance(Context context) {
        if (sInstance == null)
            sInstance = new DataBaseSource(context);
        return sInstance;
    }

    public AppDatabase getDatabase() {
        return mDatabase;
    }
}
