package com.example.myapplication2;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {

    private static final int NOTIFY_ID = 1;
    private static final String CHANNEL_ID = "Channel";
    private static final String APP_PREFERENCES = "settings";
    private static final String APP_PREFERENCES_PHONE = "phone";
    private static final int REQUEST_CODE_PERMISSION_READ_CONTACTS = 1;
    private SharedPreferences mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        createNotificationChannel();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE_PERMISSION_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectContact();
            } else {
                Toast.makeText(this, R.string.access_to_contacts_required, Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onSelectContactClick(View view) {
        int permissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS);
        if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
            selectContact();
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS},
                    REQUEST_CODE_PERMISSION_READ_CONTACTS);
        }
    }

    public void onShowContactClick(View view) {
        new ShowContactsDialog().show(getSupportFragmentManager(), "showDialog");
    }

    public void onShowFromSpClick(View view) {
        if (mSettings.contains(APP_PREFERENCES_PHONE)) {
            Snackbar.make(view, mSettings.getString(APP_PREFERENCES_PHONE, ""), Snackbar.LENGTH_LONG).show();
        }
    }

    public void onShowInNotificationClick(View view) {
        if (mSettings.contains(APP_PREFERENCES_PHONE)) {
            String phone = mSettings.getString(APP_PREFERENCES_PHONE, "");
            AppDatabase db = DataBaseSource.getInstance(this).getDatabase();
            ContactDao contactDao = db.contactDao();
            Contact contact = contactDao.getByPhone(phone);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle(phone)
                    .setContentText(contact.name + " " + contact.surname)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            notificationManager.notify(NOTIFY_ID, builder.build());
        }
    }

    public void onShowContact(@NonNull Contact contact) {
        ((TextView) findViewById(R.id.contact_name)).setText(contact.name);
        ((TextView) findViewById(R.id.contact_surname)).setText(contact.surname);
        ((TextView) findViewById(R.id.contact_phone)).setText(contact.phone);
        ((TextView) findViewById(R.id.contact_email)).setText(contact.email);

        SharedPreferences.Editor editor = mSettings.edit();
        editor.putString(APP_PREFERENCES_PHONE, contact.phone);
        editor.apply();
    }

    private void selectContact() {
        new SelectContactDialog().show(getSupportFragmentManager(), "selectDialog");
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.number_from_sp);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
