package com.example.myapplication2;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public class SelectContactDialog extends DialogFragment {

    private ContentResolver mContentResolver;

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        mContentResolver = requireActivity().getContentResolver();

        Cursor cursor = mContentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        int contactsCount = cursor.getCount();
        if (contactsCount > 0) {

            String[] contacts = new String[contactsCount];
            int i = 0;
            while (cursor.moveToNext()) {
                @SuppressLint("Range") String displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME_PRIMARY));
                contacts[i++] = displayName;
            }

            builder.setItems(contacts, (dialog, which) -> {
                cursor.moveToPosition(which);
                @SuppressLint("Range") String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                cursor.close();

                Contact contact = new Contact(getName(contactId), getSurname(contactId), getPhone(contactId), getEmail(contactId));

                AppDatabase db = DataBaseSource.getInstance(requireContext().getApplicationContext()).getDatabase();
                ContactDao contactDao = db.contactDao();
                contactDao.insert(contact);

                Toast.makeText(requireContext(), R.string.save_successfully, Toast.LENGTH_SHORT).show();
            });
        } else {
            builder.setMessage(R.string.contact_list_is_empty);
        }
        return builder.create();
    }

    @SuppressLint("Range")
    private String getName(String contactId) {
        String name = "";
        Cursor cursor = mContentResolver.query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.Data.CONTACT_ID + " = " + contactId + " AND "
                        + ContactsContract.Data.MIMETYPE + " = '" + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'",
                null,
                null);
        if (cursor.moveToFirst()) {
            name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME));
        }
        cursor.close();
        return name;
    }

    @SuppressLint("Range")
    private String getSurname(String contactId) {
        String surname = "";
        Cursor cursor = mContentResolver.query(ContactsContract.Data.CONTENT_URI,
                null,
                ContactsContract.Data.CONTACT_ID + " = " + contactId + " AND "
                        + ContactsContract.Data.MIMETYPE + " = '" + ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "'",
                null,
                null);
        if (cursor.moveToFirst()) {
            surname = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.FAMILY_NAME));
        }
        cursor.close();
        return surname;
    }

    @SuppressLint("Range")
    private String getPhone(String contactId) {
        String phone = "";
        Cursor cursor = mContentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId,
                null,
                null);
        if (cursor.moveToFirst()) {
            phone = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }
        cursor.close();
        return phone;
    }

    @SuppressLint("Range")
    private String getEmail(String contactId) {
        String email = "";
        Cursor cursor = mContentResolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " + contactId,
                null,
                null);
        if (cursor.moveToFirst()) {
            email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
        }
        cursor.close();
        return email;
    }
}
