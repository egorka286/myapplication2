package com.example.myapplication2;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import java.util.List;

public class ShowContactsDialog extends DialogFragment {

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AppDatabase db = DataBaseSource.getInstance(requireContext().getApplicationContext()).getDatabase();
        ContactDao contactDao = db.contactDao();
        List<Contact> contacts = contactDao.getAll();

        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());

        int contactsCount = contacts.size();
        if (contactsCount > 0) {

            String[] phones = new String[contactsCount];

            int i = 0;
            for (Contact contact : contacts) {
                phones[i++] = contact.phone;
            }

            builder.setItems(phones, (dialog, which) -> {
                String name = contacts.get(which).name;
                String surname = contacts.get(which).surname;
                String phone = contacts.get(which).phone;
                String email = contacts.get(which).email;
                Contact contact = new Contact(name, surname, phone, email);
                ((MainActivity) requireActivity()).onShowContact(contact);
            });
        } else {
            builder.setMessage(R.string.contact_list_is_empty);
        }
        return builder.create();
    }
}
